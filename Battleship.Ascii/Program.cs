﻿
namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Battleship.GameController;
    using Battleship.GameController.Contracts;

    internal class Program
    {

        public static int indiceColores = 0;
        public static String[] coloresMensajes = new String[5] { "DarkYellow", "Magenta", "Green", "Cyan", "Yellow" };

        private static List<Ship> myFleet;

        private static List<Ship> enemyFleet;
        private static int numTurno = 0;


        static void Main()
        {

            Console.Title = "Welcome to Battleship";

            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.WriteLine(@"|                        Welcome to Battleship                         BB-61/");
            Console.WriteLine(@" \_________________________________________________________________________|");
            Console.WriteLine();

            InitializeGame();

            StartGame();
        }

        private static void StartGame()
        {
            Console.ResetColor();

            Console.Clear();
            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");

            do
            {
                numTurno++;
                Console.Title = $@"Player, it's your turn {numTurno}";


                // PERSONA
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine();
                Console.WriteLine("Player, it's your turn");
                //Console.WriteLine("Enter coordinates for your shot :");
                Position position = null;

                //position = ParsePosition(Console.ReadLine());

                while (position == null)
                {
                    Console.WriteLine("Enter coordinates for your shot :");
                    position = ParsePosition(Console.ReadLine());
                }

                var isHit = GameController.CheckIsHit(enemyFleet, position);
                var isSink = GameController.CheckIsSink(enemyFleet, position);
                if (isHit)
                {
                    Console.Beep();

                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"";'.:..""   /");
                    Console.WriteLine(@"                  (M^^.^~~:.'"").");
                    Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                    Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                    Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");
                    Console.ResetColor();
                }

                if (isHit)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Yeah ! Nice hit !");
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Yeah ! Miss");
                    Console.ResetColor();

                }

                if (isSink)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Yeah ! You has sinked the ship!");


                    Console.WriteLine(@"____________ ? ______?____________");
                    Console.WriteLine(@" __¨_________8______8___________¨");
                    Console.WriteLine(@"___ ? _______888____888__________?");
                    Console.WriteLine(@"____88 ___888888_888888______88");
                    Console.WriteLine(@"_____8888888 ? 888888 ? 88888888");
                    Console.WriteLine(@"______8888888888888888888888");
                    Console.WriteLine(@"_______88888888888888888888");
                    Console.WriteLine(@"________ 88888888888888888");

                    Console.ResetColor();
                }


                //Console.WriteLine(isHit ? "Yeah ! Nice hit !" : "Miss");


                // COMPUTADORA
                position = GetRandomPosition();
                isHit = GameController.CheckIsHit(myFleet, position);

                Console.Title = "Computer turn";
                //Console.Clear();


                if (isHit)
                {
                    Console.WriteLine();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Computer shot in {0}{1} and has hit your ship", position.Column.ToString(), position.Row.ToString());
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine();
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Computer shot in {0}{1} and has miss your ship", position.Column.ToString(), position.Row.ToString());

                    Console.ResetColor();

                }

                //Console.WriteLine();
                //Console.WriteLine("Computer shot in {0}{1} and {2}", position.Column, position.Row, isHit ? "has hit your ship !" : "miss");
                if (isHit)
                {
                    Console.Beep();
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"";'.:..""   /");
                    Console.WriteLine(@"                  (M^^.^~~:.'"").");
                    Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                    Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                    Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");
                    Console.ResetColor();
                }
            }
            while ((GameController.CheckTotalSink(enemyFleet)) && (GameController.CheckTotalSink(myFleet)));

            if (GameController.CheckTotalSink(myFleet))
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("YOU ARE THE WINNER!!");
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("SORRY, YOU LOSE :(");
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine(@"  ________                        ________                     ");
            Console.WriteLine(@" /  _____/_____    _____   ____   \_____  \___  __ ___________ ");
            Console.WriteLine(@"/   \  ___\__  \  /     \_/ __ \   /   |   \  \/ // __ \_  __ \");
            Console.WriteLine(@"\    \_\  \/ __ \|  Y Y  \  ___/  /    |    \   /\  ___/|  | \/");
            Console.WriteLine(@"\______  (____  /__|_|  /\___  > \_______  /\_/  \___  >__|    ");
            Console.WriteLine(@"        \/     \/      \/     \/          \/          \/       ");

            Console.ReadKey();
        }

        internal static Position ParsePosition(string input)
        {
            Regex regexPos = new Regex($@"^[A-Ha-h][1-8]");

            Match match = regexPos.Match(input);

            if (!match.Success)
                return null;


            var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
            var number = int.Parse(input.Substring(1, 1));
            return new Position(letter, number);
        }

        private static Position GetRandomPosition()
        {
            int rows = 8;
            int lines = 8;
            var random = new Random();
            var letter = (Letters)random.Next(lines);
            var number = random.Next(1, 9);
            var position = new Position(letter, number);
            return position;
        }

        private static void InitializeGame()
        {
            InitializeMyFleet();

            InitializeEnemyFleet();
        }


        private static void InitializeMyFleet()
        {
            myFleet = GameController.InitializeShips().ToList();


            Console.Title = "Initialize your fleet to play!!";

            Console.WriteLine("Please position your fleet (Game board size is from A to H and 1 to 8) :");

            foreach (var ship in myFleet)
            {
                GetTextColor();
                Console.WriteLine();
                Console.WriteLine("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size);
                for (var i = 1; i <= ship.Size; i++)
                {
                    Console.WriteLine("Enter position {0} of {1} (i.e A3):", i, ship.Size);
                    string positioNRead = Console.ReadLine();
                    Position position = ParsePosition(positioNRead);
                    while (position == null || !isValidShipPosition(ship, position, myFleet))
                    {
                        Console.WriteLine("INCORRECT POSITION: Enter another position {0} of {1} (i.e A3):", i, ship.Size);
                        positioNRead = Console.ReadLine();

                        position = ParsePosition(positioNRead);
                    }

                    ship.AddPosition(positioNRead);

                }
            }
        }

        private static List<Position> GenerateRandomPositions(Ship ship)
        {
            List<Position> positions = new List<Position>();

            Position p = GetRandomPosition();

            if (isValidShipPosition(ship, p, enemyFleet))
            {
                positions.Add(p);

            }
            else
            {
                GenerateRandomPositions(ship);
            }

            for (int pos = 1; pos < ship.Size; pos++)
            {
                Position pAux = new Position();
                pAux.Row = positions.Last().Row + 1;
                pAux.Column = p.Column;

                if (isValidShipPosition(ship, pAux, enemyFleet))
                {
                    positions.Add(pAux);

                }
                else
                {
                    positions = new List<Position>();
                    break;
                }

            }

            if (!positions.Any())
            {
                GenerateRandomPositions(ship);
            }

            return positions;
        }


        private static bool isValidShipPosition(Ship ship, Position newPosition, List<Ship> fleet)
        {
            if (ship != null)
            {
                foreach (Ship shipVerify in fleet)
                {
                    foreach (var position in shipVerify.Positions)
                    {
                        if (position.Equals(newPosition))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }


        public static void GetTextColor()
        {
            if (indiceColores == coloresMensajes.Length - 1)
            {
                indiceColores = 0;
            }
            else
            {
                indiceColores++;
            }

            Console.ResetColor();
            // Get ConsoleColor from string name
            ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), coloresMensajes[indiceColores].ToString());
            // Assuming you want to set the Foreground here, not the Background
            Console.ForegroundColor = color;
        }


        private static void InitializeEnemyFleet()
        {
            enemyFleet = GameController.InitializeShips().ToList();

            foreach (Ship ship in enemyFleet)
            {
                ship.Positions = GenerateRandomPositions(ship);
            }

        }

    }
}
